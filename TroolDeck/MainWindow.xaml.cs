﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Forms;

namespace TroolDeck
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string _sResourcesFolderPath = $"{Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)}\\ResourcesDeck";
        private Dictionary<int, ButtonConfiguration> _d_ButtonConfig;

        public MainWindow()
        {
            InitializeComponent();
            InitializeButtonPics();
        }

        public Dictionary<int, ButtonConfiguration> ButtonsConfig
        {
            get => _d_ButtonConfig;
            set => _d_ButtonConfig = value;
        }

        private void InitializeButtonPics()
        {
            _d_ButtonConfig = new Dictionary<int, ButtonConfiguration>();

            // Si l'ordi n'a pas de config, on crée un dico pour init
            _d_ButtonConfig.Add(0, new ButtonConfiguration($"{this._sResourcesFolderPath}\\enorme.png", $"{_sResourcesFolderPath}\\Jamy.mp4"));
            _d_ButtonConfig.Add(1, new ButtonConfiguration($"{this._sResourcesFolderPath}\\debout.png", $"{_sResourcesFolderPath}\\debout.mp4"));
            _d_ButtonConfig.Add(2, new ButtonConfiguration($"{this._sResourcesFolderPath}\\honteux.png", $"{_sResourcesFolderPath}\\honteux.mp4"));
            _d_ButtonConfig.Add(3, new ButtonConfiguration($"{this._sResourcesFolderPath}\\oss117.png", $"{_sResourcesFolderPath}\\oss117.mp4"));
            _d_ButtonConfig.Add(4, new ButtonConfiguration($"{this._sResourcesFolderPath}\\kassosftg.png", $"{_sResourcesFolderPath}\\kassosfermetagueule.mp4"));
            _d_ButtonConfig.Add(5, new ButtonConfiguration($"{this._sResourcesFolderPath}\\marionon.png", $"{_sResourcesFolderPath}\\marionon.mp4"));
            _d_ButtonConfig.Add(6, new ButtonConfiguration($"{this._sResourcesFolderPath}\\jesuisuncon.png", $"{_sResourcesFolderPath}\\jesuisuncon.mp4"));
            _d_ButtonConfig.Add(7, new ButtonConfiguration($"{this._sResourcesFolderPath}\\cetaitsur.png", $"{_sResourcesFolderPath}\\cetaitsur.mp4"));
            _d_ButtonConfig.Add(8, new ButtonConfiguration($"{this._sResourcesFolderPath}\\fumez.png", $"{_sResourcesFolderPath}\\vousfumez.mp4"));
            _d_ButtonConfig.Add(9, new ButtonConfiguration($"{this._sResourcesFolderPath}\\rouquin.png", $"{_sResourcesFolderPath}\\rouquin.mp4"));
            _d_ButtonConfig.Add(10, new ButtonConfiguration($"{this._sResourcesFolderPath}\\souffrir.png", $"{_sResourcesFolderPath}\\Jamy.mp4"));
            _d_ButtonConfig.Add(11, new ButtonConfiguration($"{this._sResourcesFolderPath}\\ah.png", $"{_sResourcesFolderPath}\\ah.mp4"));

            ButtonBrush1.ImageSource = new BitmapImage(new Uri(_d_ButtonConfig[0].Picture));
            ButtonBrush2.ImageSource = new BitmapImage(new Uri(_d_ButtonConfig[1].Picture));
            ButtonBrush3.ImageSource = new BitmapImage(new Uri(_d_ButtonConfig[2].Picture));
            ButtonBrush4.ImageSource = new BitmapImage(new Uri(_d_ButtonConfig[3].Picture));
            ButtonBrush5.ImageSource = new BitmapImage(new Uri(_d_ButtonConfig[4].Picture));
            ButtonBrush6.ImageSource = new BitmapImage(new Uri(_d_ButtonConfig[5].Picture));
            ButtonBrush7.ImageSource = new BitmapImage(new Uri(_d_ButtonConfig[6].Picture));
            ButtonBrush8.ImageSource = new BitmapImage(new Uri(_d_ButtonConfig[7].Picture));
            ButtonBrush9.ImageSource = new BitmapImage(new Uri(_d_ButtonConfig[8].Picture));
            ButtonBrush10.ImageSource = new BitmapImage(new Uri(_d_ButtonConfig[9].Picture));
            ButtonBrush11.ImageSource = new BitmapImage(new Uri(_d_ButtonConfig[10].Picture));
            ButtonBrush12.ImageSource = new BitmapImage(new Uri(_d_ButtonConfig[11].Picture));
        }

        private string GetFile()
        {
            string sRet = string.Empty;
            OpenFileDialog ofd = new OpenFileDialog()
            {
                Title = "Select a Meme Video",
                Filter = "Video File (*.mp4) | *.mp4",
                FileName = string.Empty
            };

            return sRet;
        }

        //=========================================================================================
        #region Events

        #region Launch meme video

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri(ButtonsConfig[0].Video, UriKind.Absolute);
            Video vid = new Video(uri);
            vid.Show();            
        }
        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri(ButtonsConfig[1].Video, UriKind.Absolute);
            Video vid = new Video(uri);
            vid.Show();
        }
        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri(ButtonsConfig[2].Video, UriKind.Absolute);
            Video vid = new Video(uri);
            vid.Show();
        }
        private void Button4_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri(ButtonsConfig[3].Video, UriKind.Absolute);
            Video vid = new Video(uri);
            vid.Show();
        }
        private void Button5_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri(ButtonsConfig[4].Video, UriKind.Absolute);
            Video vid = new Video(uri);
            vid.Show();
        }
        private void Button6_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri(ButtonsConfig[5].Video, UriKind.Absolute);
            Video vid = new Video(uri);
            vid.Show();
        }
        private void Button7_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri(ButtonsConfig[6].Video, UriKind.Absolute);
            Video vid = new Video(uri);
            vid.Show();
        }
        private void Button8_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri(ButtonsConfig[7].Video, UriKind.Absolute);
            Video vid = new Video(uri);
            vid.Show();
        }
        private void Button9_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri(ButtonsConfig[8].Video, UriKind.Absolute);
            Video vid = new Video(uri);
            vid.Show();
        }
        private void Button10_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri(ButtonsConfig[9].Video, UriKind.Absolute);
            Video vid = new Video(uri);
            vid.Show();
        }
        private void Button11_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri(ButtonsConfig[10].Video, UriKind.Absolute);
            Video vid = new Video(uri);
            vid.Show();
        }
        private void Button12_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri(ButtonsConfig[11].Video, UriKind.Absolute);
            Video vid = new Video(uri);
            vid.Show();
        }

        #endregion

        #region Change Pic Button
        #endregion

        #region Change Meme Video

        private void ChangeMemeButton1(object sender, RoutedEventArgs e)
        {
            string sVidMeme = this.GetFile();
            Dictionary<int, ButtonConfiguration> config = new Dictionary<int, ButtonConfiguration>();
            config[0].Video = sVidMeme;
            Serialization.Save(config);
        }
        private void ChangeMemeButton2(object sender, RoutedEventArgs e)
        {
        }
        private void ChangeMemeButton3(object sender, RoutedEventArgs e)
        {
        }
        private void ChangeMemeButton4(object sender, RoutedEventArgs e)
        {
        }
        private void ChangeMemeButton5(object sender, RoutedEventArgs e)
        {
        }
        private void ChangeMemeButton6(object sender, RoutedEventArgs e)
        {
        }
        private void ChangeMemeButton7(object sender, RoutedEventArgs e)
        {
        }
        private void ChangeMemeButton8(object sender, RoutedEventArgs e)
        {
        }
        private void ChangeMemeButton9(object sender, RoutedEventArgs e)
        {
        }
        private void ChangeMemeButton10(object sender, RoutedEventArgs e)
        {
        }
        private void ChangeMemeButton11(object sender, RoutedEventArgs e)
        {
        }
        private void ChangeMemeButton12(object sender, RoutedEventArgs e)
        {
        }

        #endregion

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void OnClickButtonQuit(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TroolDeck
{
    [Serializable]
    public class ButtonConfiguration
    {
        //=========================================================================================
        #region Members

        private string _sPicFilePath;
        private string _sVidFilePath;

        #endregion

        //=========================================================================================
        #region Accessors

        public string Picture
        {
            get => this._sPicFilePath;
            set => this._sPicFilePath = value;
        }
        public string Video
        {
            get => this._sVidFilePath;
            set => this._sVidFilePath = value;
        }

        #endregion

        //=========================================================================================
        #region Methods

        public ButtonConfiguration(string sPicture, string sVideo)
        {
            Picture = sPicture;
            Video = sVideo;
        }

        #endregion
    }
}

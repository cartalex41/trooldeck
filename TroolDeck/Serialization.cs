﻿using System;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;

namespace TroolDeck
{
    public static class Serialization
    {
        private static string _sConfigFilePath = "SaveConfig.bin";

        public static void Save(Dictionary<int, ButtonConfiguration> d_configuration)
        {
            FileStream fs = File.Create(_sConfigFilePath);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, d_configuration);
            fs.Close();
        }

        public static Dictionary<int, ButtonConfiguration> ReadConfig()
        {
            Dictionary<int, ButtonConfiguration> configRet = new Dictionary<int, ButtonConfiguration>();



            return configRet;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TroolDeck
{
    /// <summary>
    /// Logique d'interaction pour Video.xaml
    /// </summary>
    public partial class Video : Window
    {
        public Video(Uri videoUri)
        {
            InitializeComponent();
            MediaElementVideo.Source = videoUri;
        }

        void OnMouseDownClose(object sender, MouseButtonEventArgs args)
        {
            this.Close();
        }

        void OnMediaEnded(object sender, RoutedEventArgs args)
        {
            this.Close();
        }
    }
}
